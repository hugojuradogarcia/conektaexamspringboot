package com.conekta.antifraud.api.dao;

import com.conekta.antifraud.api.model.entity.FinancialEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialEntityRepository extends JpaRepository<FinancialEntity, Integer> {
    FinancialEntity findByFinancialName(String name);

    @Query(value = "SELECT financial_name " +
            "FROM financial_entity " +
            "WHERE financial_name LIKE ?1 " +
            "AND financial_entity_type_id = ?2", nativeQuery = true)
    String findByFinancialNameAndFinancialEntityTypeId(String name, int id);

    FinancialEntity findFirstByFinancialName(String name);
}
