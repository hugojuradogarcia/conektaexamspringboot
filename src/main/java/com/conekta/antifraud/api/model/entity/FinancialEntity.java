package com.conekta.antifraud.api.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "financial_entity")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class FinancialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_financial_entity", nullable = false)
    private int idFinancialEntity;

    @NotNull(message = "financialName - no puede ser null")
    @NotEmpty(message = "financialName - no puede estar vacío")
    @Size(min = 1, max = 60, message = "financialName - máximo 60 caracteres")
    @Column(name = "financial_name", nullable = false)
    private String financialName;

    @NotNull(message = "enabled - no puede ser null")
    @NotEmpty(message = "enabled - no puede estar vacío")
    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "financial_entity_type_id")
    @JsonBackReference
    private FinancialEntityType financialEntityType;

    public void setFinancialEntityType(FinancialEntityType financialEntityType) {
        this.financialEntityType = financialEntityType;
    }

    public FinancialEntity(@NotNull(message = "financialName - no puede ser null") @NotEmpty(message = "financialName - no puede estar vacío") @Size(min = 1, max = 60, message = "financialName - máximo 60 caracteres") String financialName) {
        this.financialName = financialName;
    }
}
