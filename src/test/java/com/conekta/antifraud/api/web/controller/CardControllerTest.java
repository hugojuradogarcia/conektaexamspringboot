package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Card;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CardControllerTest {

    private static final String CLIENT_ID = "client";
    private static final String CLIENT_SECRET = "secret";
    private static final String USER_ID = "username";
    private static final String USER_SECRET = "password";

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    public void updateCard_errorValidateCardNumberLuhn() throws Exception {
        String idCard = "1";
        Card card = new Card("120000.00", "10000.00", "21212121212");

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(card);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(put("/user/updateCard/" + idCard + "?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateCard_errorValidateAmountCreditAndAmountCreditUse() throws Exception {
        String idCard = "1";
        Card card = new Card("120000", "10000.000", "5062210136766663");

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(card);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(put("/user/updateCard/" + idCard + "?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    // Obtenemos el token generado
    public String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic "
                + new String(Base64Utils.encode((CLIENT_ID + ":" + CLIENT_SECRET).getBytes()));

        String content = mockMvc.perform(post("/oauth/token")
                .contentType(
                        MediaType.APPLICATION_FORM_URLENCODED)
                .param("grant_type", "password")
                .param("username", username)
                .param("password", password)
                .header("Authorization", authorization))
                .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }
}