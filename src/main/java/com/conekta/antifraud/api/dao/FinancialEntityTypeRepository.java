package com.conekta.antifraud.api.dao;

import com.conekta.antifraud.api.model.entity.FinancialEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialEntityTypeRepository extends JpaRepository<FinancialEntityType, Integer> {
    FinancialEntityType findByFinancialEntityTypeName(String name);
}
