package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Cardholder;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.service.CardholderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

@RestController
@Api(description = "USER ONLY ACCESS - Save, Update & Delete Cardholder")
public class CardholderController {

    @Autowired
    private CardholderService cardholderService;

    @PostMapping("/user/saveCardholder")
    @ApiOperation(value = "Save Cardholder")
    public @ResponseBody
    ResponseEntity<Response> saveCardholder(@RequestBody @Validated Cardholder request){
        return cardholderService.saveCardholder(request);
    }

    @PutMapping("/user/updateCardholder/{id}")
    @ApiOperation(value = "Update Cardholder")
    public @ResponseBody
    ResponseEntity<Response> updateCardholder(@PathVariable @Validated @Size(min = 1, message = "idCardHolder - únicamente números, mínimo 1 número") int id,
                                              @RequestBody @Validated Cardholder request){
        return cardholderService.updateCardholder(id, request);
    }

    @DeleteMapping("/user/deleteCardholder/{id}")
    @ApiOperation(value = "Delete Cardholder")
    public @ResponseBody
    ResponseEntity<Response> deleteCardholder(@PathVariable @Validated @Size(min = 1, message = "idCardHolder - únicamente números, mínimo 1 número")int id){
        return cardholderService.deleteCardholder(id);
    }
}
