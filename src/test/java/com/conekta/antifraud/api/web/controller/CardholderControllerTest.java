package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Card;
import com.conekta.antifraud.api.model.entity.Cardholder;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CardholderControllerTest {

    private static final String CLIENT_ID = "client";
    private static final String CLIENT_SECRET = "secret";
    private static final String USER_ID = "username";
    private static final String USER_SECRET = "password";

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    public void saveCardholder_errorValidateEmailAndRfc() throws Exception {

        Cardholder cardholder = new Cardholder();
        cardholder.setFinancialEntityId(2);
        cardholder.setAddress("ERMITA");
        cardholder.setEmail("hugogmail");
        cardholder.setHomePhone("21436576");
        cardholder.setMobilePhone("5521436576");
        cardholder.setName("LEONARDO JURADO");
        cardholder.setRfc("JUGH");

        Card card = new Card("12000.00", "1000.00","4532123312435676");
        cardholder.addCard(card);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(cardholder);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/user/saveCardholder?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void saveCardholder_errorValidateHomeAndMobilePhone() throws Exception {

        Cardholder cardholder = new Cardholder();
        cardholder.setFinancialEntityId(2);
        cardholder.setAddress("ERMITA");
        cardholder.setEmail("hugo@gmail");
        cardholder.setHomePhone("21436576wq");
        cardholder.setMobilePhone("5521436576wq");
        cardholder.setName("LEONARDO JURADO");
        cardholder.setRfc("JUGH880821DC2");

        Card card = new Card("12000.00", "1000.00","4532123312435676");
        cardholder.addCard(card);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(cardholder);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/user/saveCardholder?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateCardholder() throws Exception {
        String idCardHolder = "12as";
        Cardholder cardholder = new Cardholder();
        cardholder.setFinancialEntityId(2);
        cardholder.setAddress("ERMITA");
        cardholder.setEmail("hugo@gmail.com");
        cardholder.setHomePhone("21436576");
        cardholder.setMobilePhone("5521436576");
        cardholder.setName("LEONARDO JURADO");
        cardholder.setRfc("JUGH880821DC2");

        Card card = new Card("12000.00", "1000.00","5062210136766663");
        cardholder.addCard(card);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(cardholder);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(put("/user/updateCardholder/" + idCardHolder + "?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    // Obtenemos el token generado
    public String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic "
                + new String(Base64Utils.encode((CLIENT_ID + ":" + CLIENT_SECRET).getBytes()));

        String content = mockMvc.perform(post("/oauth/token")
                .contentType(
                        MediaType.APPLICATION_FORM_URLENCODED)
                .param("grant_type", "password")
                .param("username", username)
                .param("password", password)
                .header("Authorization", authorization))
                .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }
}