package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.FinancialEntity;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.service.FinancialEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

@RestController
@Api(description = "ADMIN ONLY ACCESS - Update & Delete Financial Entity")
public class FinancialEntityController {

    @Autowired
    private FinancialEntityService financialEntityService;

    @PutMapping("/admin/updateFinancialEntity/{id}")
    @ApiOperation(value = "Update Financial Entity")
    public @ResponseBody
    ResponseEntity<Response> updateFinancialEntity(@PathVariable @Validated @Size(min = 1, message = "idFinancialEntity - únicamente números, mínimo 1 número") int id,
                                                   @RequestBody @Validated FinancialEntity request){
        return financialEntityService.updateFinancialEntity(id, request);
    }

    @DeleteMapping("/admin/deleteFinancialEntity/{id}")
    @ApiOperation(value = "Delete Financial Entity By Id")
    public @ResponseBody
    ResponseEntity<Response> deleteFinancialEntity(@PathVariable @Validated @Size(min = 1, message = "idFinancialEntity - únicamente números, mínimo 1 número") int id){
            return financialEntityService.deleteFinancialEntity(id);
    }
}
