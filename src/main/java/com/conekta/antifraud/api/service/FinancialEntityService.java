package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.model.entity.FinancialEntity;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.http.ResponseEntity;

public interface FinancialEntityService {
    ResponseEntity<Response> updateFinancialEntity(int id, FinancialEntity financialEntity);
    ResponseEntity<Response> deleteFinancialEntity(int id);
}
