package com.conekta.antifraud.api.model.entity.exception.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StatusReportValid implements ConstraintValidator<StatusReportConstraint, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s.equals("A tiempo") ||
                s.equals("Con retraso menor a 10 días") ||
                s.equals("Con retraso mayor a 10 días") ||
                s.equals("Con falta de pago")) {
            return true;
        }
        return false;
    }
}
