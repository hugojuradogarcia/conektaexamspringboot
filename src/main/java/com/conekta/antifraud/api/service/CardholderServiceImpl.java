package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.CardRepository;
import com.conekta.antifraud.api.dao.CardholderRepository;
import com.conekta.antifraud.api.dao.FinancialEntityRepository;
import com.conekta.antifraud.api.model.commons.Utils;
import com.conekta.antifraud.api.model.entity.Card;
import com.conekta.antifraud.api.model.entity.Cardholder;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CardholderServiceImpl implements CardholderService {

    @Autowired
    private Utils utils;

    @Autowired
    private CardholderRepository cardholderRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public ResponseEntity<Response> saveCardholder(Cardholder cardholder) {

        // Validamos si existe la llave foranea a asociar
        if (financialEntityRepository.findById(cardholder.getFinancialEntityId()) != null) {
            // Validamos si exite un tarjetahabiente con el mismo nombre y rfc
            Cardholder cardHolderExists = cardholderRepository.findFirstByNameAndRfc(cardholder.getName(), cardholder.getRfc());
            List<Card> cardsSave = new ArrayList<>();
            // Validamos si existe el mismo numero de tarjeta
            for (Card card : cardholder.getCards()
            ) {
                if (cardRepository.findFirstByCardNumber(card.getCardNumber()) == null) {
                    cardsSave.add(card);
                }
            }

            if (cardHolderExists == null) {
                cardholder.getCards().clear();
                for (Card card : cardsSave
                ) {
                    cardholder.addCard(card);
                }

                Cardholder cardholderSave = cardholderRepository.save(cardholder);
                return new ResponseEntity<>(utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString()), HttpStatus.OK);
            }

            return new ResponseEntity<>(utils.RESPONSE_EXIST(HttpStatus.OK.toString()), HttpStatus.OK);
        }
        return new ResponseEntity<>(utils.RESPONSE_ERROR_ASSOCIATION(HttpStatus.OK.toString()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> updateCardholder(int id, Cardholder cardholder) {
        Optional<Cardholder> cardholderExists = cardholderRepository.findById(id);

        if (cardholderExists.isPresent()){
            cardholder.setIdCardHolder(id);
            cardholderRepository.save(cardholder);
            return new ResponseEntity<>(utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.OK.toString()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> deleteCardholder(int id) {
        Optional<Cardholder> cardholderExists = cardholderRepository.findById(id);

        if (cardholderExists.isPresent()){
            cardholderRepository.deleteById(id);
            return new ResponseEntity<>(utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.OK.toString()), HttpStatus.OK);
    }
}
