package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.UserRepository;
import com.conekta.antifraud.api.model.entity.security.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        com.conekta.antifraud.api.model.entity.security.User user = userRepository
                .findByUsername(username);

        return new User(user.getUsername(), user.getPassword(),
                user.isActive(), user.isActive(), user.isActive(), user.isActive(), buildUserAuthority(user.getRoles()));

    }

    private List<GrantedAuthority> buildUserAuthority(Set<Role> roles) {

        Set<GrantedAuthority> setAuths = new HashSet<>();

        // add user's authorities
        for (Role role : roles) {
            setAuths.add(new SimpleGrantedAuthority(role.getName()));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

        return Result;
    }

}
