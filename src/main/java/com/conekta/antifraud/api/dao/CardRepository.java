package com.conekta.antifraud.api.dao;

import com.conekta.antifraud.api.model.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
    Card findFirstByCardNumber(String cardNumber);
}
