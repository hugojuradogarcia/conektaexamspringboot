package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.model.entity.Report;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.http.ResponseEntity;

public interface ReportService {
    ResponseEntity<Response> saveReport(Report report);
    ResponseEntity<Response> findByRfcAndCardNumberFinal(String rfc, String carNumberFinal);
    ResponseEntity<Response> findByRfc(String rfc);
    ResponseEntity<Response> findByCardNumberFinal(String carNumberFinal);
}
