package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.CardRepository;
import com.conekta.antifraud.api.model.commons.Utils;
import com.conekta.antifraud.api.model.entity.Card;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private Utils utils;

    @Autowired
    private CardRepository cardRepository;

    @Override
    public ResponseEntity<Response> updateCard(int id, Card card) {
        Optional<Card> cardExists = cardRepository.findById(id);

        if (cardExists.isPresent()){
            card.setIdCard(id);
            cardRepository.save(card);
            return new ResponseEntity<>(utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> deleteCard(int id) {
        Optional<Card> cardExists = cardRepository.findById(id);

        if (cardExists.isPresent()){
            cardRepository.deleteById(id);
            return new ResponseEntity<>(utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
}
