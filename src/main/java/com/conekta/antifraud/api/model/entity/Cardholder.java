package com.conekta.antifraud.api.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "cardholder")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class Cardholder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_card_holder", nullable = false)
    private int idCardHolder;

    @NotNull(message = "name - no puede ser null")
    @NotEmpty(message = "name - no puede estar vacío")
    @Size(min = 1, max = 60, message = "name - máximo 60 caracteres")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(message = "email - no puede ser null")
    @NotEmpty(message = "email - no puede estar vacío")
    @Size(max = 100, message = "email - máximo 100 caracteres")
    @Email(message = "email - debe ser un email valido")
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "home_phone")
    @Pattern(regexp="(^$|[0-9]{0,13})", message = "homePhone - únicamente números, máximo 13")
    private String homePhone;

    @Column(name = "mobile_phone")
    @Pattern(regexp="(^$|[0-9]{0,13})", message = "mobilePhone - únicamente números, máximo 13")
    private String mobilePhone;

    @NotNull
    @Pattern(regexp = "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$",  message = "rfc - debe de ser un rfc valido")
    @Column(name = "rfc", nullable = false)
    private String rfc;

    @NotNull(message = "address - no puede ser null")
    @NotEmpty(message = "address - no puede estar vacío")
    @Size(max = 200, message = "address - máximo 16 caracteres")
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_At", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @Column(name = "financial_entity_id")
    private int financialEntityId ;

    @Valid
    @OneToMany(mappedBy = "cardholder", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<Card> cards;

    public void addCard(Card card){
        if (card == null){
            return;
        }
        card.setCardholder(this);
        if (cards == null){
            cards = new ArrayList<>();
            cards.add(card);
        }else if (!cards.contains(card)){
            cards.add(card);
        }
    }
}
