package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.ConektaApplication;
import com.conekta.antifraud.api.model.entity.FinancialEntity;
import com.conekta.antifraud.api.model.entity.FinancialEntityType;
import com.conekta.antifraud.api.service.FinancialEntityTypeService;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = ConektaApplication.class)
public class FinancialEntityTypeControllerTest {

    private static final String CLIENT_ID = "client";
    private static final String CLIENT_SECRET = "secret";
    private static final String USER_ID = "username";
    private static final String USER_SECRET = "password";

    @Autowired
    WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @MockBean
    public FinancialEntityTypeService financialEntityTypeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void saveFinancialEntityTypeAll() throws Exception {

        FinancialEntityType financialEntityType = new FinancialEntityType();
        financialEntityType.setFinancialEntityTypeName("PRUEBA");

        List<FinancialEntity> financialEntities = new ArrayList<>();
        FinancialEntity financialEntity = new FinancialEntity();
        financialEntity.setEnabled(true);
        financialEntity.setFinancialName("INBURSA");

        financialEntityType.addFinancialEntity(financialEntity);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(financialEntityType);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/admin/saveFinancialEntityTypeAll?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void saveFinancialEntityType_errorNotNullEntityTypeName() throws Exception {
        FinancialEntity financialEntity = new FinancialEntity(null);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(financialEntity);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/admin/saveFinancialEntityType?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }
    @Test
    public void updateFinancialEntityType_errorValidateId() throws Exception {
        FinancialEntityType financialEntityType = new FinancialEntityType("INBURSA");
        String idFinancialEntityType = "12a";

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(financialEntityType);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(put("/admin/updateFinancialEntityType/" + idFinancialEntityType + "?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    // Obtenemos el token generado
    public String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic "
                + new String(Base64Utils.encode((CLIENT_ID + ":" + CLIENT_SECRET).getBytes()));

        String content = mockMvc.perform(post("/oauth/token")
                .contentType(
                        MediaType.APPLICATION_FORM_URLENCODED)
                .param("grant_type", "password")
                .param("username", username)
                .param("password", password)
                .header("Authorization", authorization))
                .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }

    /*@Test
    public void saveFinancialEntityType() {
    }

    @Test
    public void updateFinancialEntityType() {
    }*/
}