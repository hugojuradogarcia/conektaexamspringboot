# Conekta API RESTful 
API RESTful para gestionar un buró de crédito (Microservice with Spring Boot)

## Spring Boot 2 + Spring Security + OAuth2

## Getting started
### Prerequisites:
- Java 8
- Maven
- MySQL 

## Hosting
- Instance: EC2 
- RDS: MySQL 
- Operating System: Ubuntu 16.04 
- Server: embedded tomcat
- Host: [http://3.16.156.59:8181/](http://3.16.156.59:8181/)

## Structure of the code ##
    Package
        - Api 
            - web (CONTROLLER, CONFIG FILES & GlobalException)
            - dao (REPOSITORY)
            - model (MODEL, ENTITY, CUSTOM VALIDATE & UTILS)
            - service (LOGIC BUSSINES)
        

## Deploy

Installation as an init.d service

FTP

Upload file (server) conekta-0.0.1-SNAPSHOT.jar into the path: /home/ubuntu

```
$ sudo mv conekta-0.0.1-SNAPSHOT.jar service/
$ chmod 500 conektaApi.jar
& sudo ln -s /home/ubuntu/service/conektaApi.jar /etc/init.d/conektaApi
& sudo service conektaApi start
```

## Generate packaging (WAR) for deploy into server
```
$ mvn update
$ mvn clean install 
```

## Authentication
* Authentication type :OAUTH2 

```
Basic Auth
    Username: client
    Password: secret
Params ContentType x-www-form.urlencoded
    username: username
    password: password
    grant_type: password
```
  
## Collection Postman
* [Swagger](http://3.16.156.59:8181/swagger-ui.html)

## Collection Postman
* [Conekta Exam Collection](https://www.getpostman.com/collections/a2713d4baadfc4a51e32)

## Authors
* **Hugo Jurado** - *Repository* - [ConektaExam](https://bitbucket.org/hugojuradogarcia/conektaexamspringboot/src)