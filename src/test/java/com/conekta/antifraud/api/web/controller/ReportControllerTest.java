package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Report;
import com.conekta.antifraud.api.service.ReportService;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReportControllerTest {

    private static final String CLIENT_ID = "client";
    private static final String CLIENT_SECRET = "secret";
    private static final String USER_ID = "username";
    private static final String USER_SECRET = "password";

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private ReportService reportService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    // Prueba que no permite un estado invalido en el request
    @WithMockUser(value = "client")
    @Test
    public void saveReport_errorBadRequestByStatus() throws Exception {
        Report report = new Report("A tiempoa", 1);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(report);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/user/report/saveReport?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    // Prueba foreing key no asociado
    @Test
    public void saveReport_errorNotAssocitationFK() throws Exception {
        Report report = new Report("A tiempo", 1221);

        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_OBJECT = objectMapper.writeValueAsString(report);

        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(post("/user/report/saveReport?access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_OBJECT))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void findByRfcAndCardNumberFinal_errorValidateRfcAndCardNumber() throws Exception {
        String rfc = "JUGH880821DC2s";
        String cardNumber = "2121a";
        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(get("/user/report/findByRfcAndCardNumberFinal?rfc=" + rfc + "&cardNumber=" + cardNumber + "&access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void findByRfc_errorValidateRfc() throws Exception {
        String rfc = "JUGH880821DC2s";
        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(get("/user/report/findByRfc?rfc=" + rfc + "&access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void findByCardNumberFinal_erroValidateCardNumber() throws Exception {
        String cardNumber = "2121a";
        String accessToken = getAccessToken("username", "password");

        mockMvc.perform(get("/user/report/findByCardNumberFinal?cardNumber=" + cardNumber + "&access_token=" + accessToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    // Obtenemos el token generado
    public String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic "
                + new String(Base64Utils.encode((CLIENT_ID + ":" + CLIENT_SECRET).getBytes()));

        String content = mockMvc.perform(post("/oauth/token")
                .contentType(
                        MediaType.APPLICATION_FORM_URLENCODED)
                .param("grant_type", "password")
                .param("username", username)
                .param("password", password)
                .header("Authorization", authorization))
                .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }
}