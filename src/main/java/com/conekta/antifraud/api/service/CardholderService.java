package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.model.entity.Cardholder;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.http.ResponseEntity;

public interface CardholderService {
    ResponseEntity<Response> saveCardholder(Cardholder cardholder);
    ResponseEntity<Response> updateCardholder(int id, Cardholder cardholder);
    ResponseEntity<Response> deleteCardholder(int id);
}
