package com.conekta.antifraud.api.model.entity.exception.customvalidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StatusReportValid.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface StatusReportConstraint {
    String message() default "status - Status no valido únicamente los valores[A tiempo], [Con retraso menor a 10 días], [Con retraso mayor a 10 días] o [Con falta de pago] ] ";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
