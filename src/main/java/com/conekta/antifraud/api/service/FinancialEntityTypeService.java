package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.model.entity.FinancialEntityType;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface FinancialEntityTypeService {
    ResponseEntity<Response> saveFinancialEntityTypeAll(FinancialEntityType financialEntityType);
    ResponseEntity<Response> updateFinancialEntityType(int id, FinancialEntityType financialEntityType);
    ResponseEntity<Response> saveFinancialEntityType(FinancialEntityType financialEntityType);
    boolean findByName(String name);
}
