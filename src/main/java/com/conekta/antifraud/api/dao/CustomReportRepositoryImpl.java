package com.conekta.antifraud.api.dao;

import com.conekta.antifraud.api.model.out.ResponseReport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class CustomReportRepositoryImpl implements CustomReportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    // posicion dentro del objeto generico
    private static final int AMOUNT_CREDIT = 0;
    private static final int AMOUNT_CREDIT_USE = 1;
    private static final int CREATE_AT = 2;
    private static final int FINANCIAL_NAME = 3;
    private static final int SCORE = 5;

    @Transactional
    @Override
    public ResponseReport findByRfcAndCardNumberFinal(String rfc, String cardNumberFinal) {

        List<ResponseReport> responseReports =  new ArrayList<>();

        String sql = "SELECT " +
                "       C.amount_credit," +
                "       C.amount_credit_use, " +
                "       R.created_at, " +
                "       FE.financial_name, " +
                "       COUNT(R.status) STA, " +
                "       IF(status = 'A tiempo', 'BUENA', " +
                "         (IF(status = 'Con retraso menor a 10 días', 'PASABLE', " +
                "             (IF(status = 'Con retraso mayor a 10 días', 'MALA', 'MUY MALA'))))) as STATUS " +
                "       FROM cardholder CH " +
                "           INNER JOIN card C " +
                "               ON C.card_holder_id = CH.id_card_holder " +
                "           INNER JOIN financial_entity FE " +
                "               ON CH.financial_entity_id = FE.id_financial_entity " +
                "           INNER JOIN report R " +
                "               ON CH.id_card_holder = R.card_holder_id " +
                "           WHERE CH.rfc LIKE ?1 " +
                "               AND C.card_number LIKE ?2 " +
                "               AND R.created_at >= (NOW() - INTERVAL 1 MONTH) " +
                "           GROUP BY R.status " +
                "           ORDER BY STA desc " +
                "           LIMIT 1";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, rfc);
        query.setParameter(2, "%" + cardNumberFinal);

        List<Object[]> listObject = query.getResultList();
        if (listObject != null || !listObject.isEmpty()){
            for (Object[] response:
                    listObject){
                ResponseReport responseReportBuilder = ResponseReport.builder()
                        .amountCredit((String) response[AMOUNT_CREDIT])
                        .amountCreditUse((String) response[AMOUNT_CREDIT_USE])
                        .createdAt((Date) response[CREATE_AT])
                        .financialName((String) response[FINANCIAL_NAME])
                        .score((String) response[SCORE])
                        .build();

                responseReports.add(responseReportBuilder);
            }
            if (responseReports.size() > 0) {
                return responseReports.get(0);
            }
        }
        return null;
    }

    @Transactional
    @Override
    public ResponseReport findByRfc(String rfc) {

        List<ResponseReport> responseReports =  new ArrayList<>();

        String sql = "SELECT " +
                "       C.amount_credit," +
                "       C.amount_credit_use, " +
                "       R.created_at, " +
                "       FE.financial_name, " +
                "       COUNT(R.status) STA, " +
                "       IF(status = 'A tiempo', 'BUENA', " +
                "         (IF(status = 'Con retraso menor a 10 días', 'PASABLE', " +
                "             (IF(status = 'Con retraso mayor a 10 días', 'MALA', 'MUY MALA'))))) as STATUS " +
                "       FROM cardholder CH " +
                "           INNER JOIN card C " +
                "               ON C.card_holder_id = CH.id_card_holder " +
                "           INNER JOIN financial_entity FE " +
                "               ON CH.financial_entity_id = FE.id_financial_entity " +
                "           INNER JOIN report R " +
                "               ON CH.id_card_holder = R.card_holder_id " +
                "           WHERE CH.rfc LIKE ?1 " +
                "           AND R.created_at >= (NOW() - INTERVAL 1 MONTH) " +
                "           GROUP BY R.status " +
                "           ORDER BY STA desc " +
                "           LIMIT 1";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, rfc);

        List<Object[]> listObject = query.getResultList();
        if (listObject != null || !listObject.isEmpty()){
            for (Object[] response:
                    listObject){
                ResponseReport responseReportBuilder = ResponseReport.builder()
                        .amountCredit((String) response[AMOUNT_CREDIT])
                        .amountCreditUse((String) response[AMOUNT_CREDIT_USE])
                        .createdAt((Date) response[CREATE_AT])
                        .financialName((String) response[FINANCIAL_NAME])
                        .score((String) response[SCORE])
                        .build();

                responseReports.add(responseReportBuilder);
            }
            if (responseReports.size() > 0) {
                return responseReports.get(0);
            }
        }
        return null;
    }

    @Transactional
    @Override
    public ResponseReport findByCardNumberFinal(String cardNumberFinal) {

        List<ResponseReport> responseReports =  new ArrayList<>();

        String sql = "SELECT " +
                "       C.amount_credit," +
                "       C.amount_credit_use, " +
                "       R.created_at, " +
                "       FE.financial_name, " +
                "       COUNT(R.status) STA, " +
                "       IF(status = 'A tiempo', 'BUENA', " +
                "         (IF(status = 'Con retraso menor a 10 días', 'PASABLE', " +
                "             (IF(status = 'Con retraso mayor a 10 días', 'MALA', 'MUY MALA'))))) as STATUS " +
                "       FROM cardholder CH " +
                "           INNER JOIN card C " +
                "               ON C.card_holder_id = CH.id_card_holder " +
                "           INNER JOIN financial_entity FE " +
                "               ON CH.financial_entity_id = FE.id_financial_entity " +
                "           INNER JOIN report R " +
                "               ON CH.id_card_holder = R.card_holder_id " +
                "           WHERE C.card_number LIKE ?1 " +
                "               AND R.created_at >= (NOW() - INTERVAL 1 MONTH) " +
                "           GROUP BY R.status " +
                "           ORDER BY STA desc " +
                "           LIMIT 1";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, "%" + cardNumberFinal);

        List<Object[]> listObject = query.getResultList();
        if (listObject != null || !listObject.isEmpty()){
            for (Object[] response:
                    listObject){
                ResponseReport responseReportBuilder = ResponseReport.builder()
                        .amountCredit((String) response[AMOUNT_CREDIT])
                        .amountCreditUse((String) response[AMOUNT_CREDIT_USE])
                        .createdAt((Date) response[CREATE_AT])
                        .financialName((String) response[FINANCIAL_NAME])
                        .score((String) response[SCORE])
                        .build();

                responseReports.add(responseReportBuilder);
            }
            if (responseReports.size() > 0) {
                return responseReports.get(0);
            }
        }
        return null;
    }
}
