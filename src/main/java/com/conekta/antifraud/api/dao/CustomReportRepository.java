package com.conekta.antifraud.api.dao;

import com.conekta.antifraud.api.model.out.ResponseReport;

public interface CustomReportRepository {
    ResponseReport findByRfcAndCardNumberFinal(String rfc, String cardNumberFinal);
    ResponseReport findByRfc(String rfc);
    ResponseReport findByCardNumberFinal(String cardNumberFinal);
}
