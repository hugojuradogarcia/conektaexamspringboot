package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.CardholderRepository;
import com.conekta.antifraud.api.dao.CustomReportRepository;
import com.conekta.antifraud.api.dao.ReportRepository;
import com.conekta.antifraud.api.model.commons.Utils;
import com.conekta.antifraud.api.model.entity.Report;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.model.out.ResponseReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private Utils utils;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private CustomReportRepository customReportRepository;

    @Autowired
    private CardholderRepository cardholderRepository;

    @Override
    public ResponseEntity<Response> saveReport(Report report) {
        // Validamos si existe la llave foranea a asociar
        if (cardholderRepository.findById(report.getCardHolderId()).isPresent()) {
            reportRepository.save(report);
            return new ResponseEntity<>(utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString()), HttpStatus.OK);
        }
        return new ResponseEntity<>(utils.RESPONSE_ERROR_ASSOCIATION(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> findByRfcAndCardNumberFinal(String rfc, String carNumberFinal) {
        ResponseReport responseReport = customReportRepository.findByRfcAndCardNumberFinal(rfc, carNumberFinal);

        if (responseReport != null){
            return new ResponseEntity<>(utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> findByRfc(String rfc) {
        ResponseReport responseReport = customReportRepository.findByRfc(rfc);

        if (responseReport != null){
            return new ResponseEntity<>(utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> findByCardNumberFinal(String carNumberFinal) {
        ResponseReport responseReport = customReportRepository.findByCardNumberFinal(carNumberFinal);

        if (responseReport != null){
            return new ResponseEntity<>(utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
}
