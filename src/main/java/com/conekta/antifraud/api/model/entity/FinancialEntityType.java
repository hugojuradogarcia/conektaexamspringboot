package com.conekta.antifraud.api.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "financial_entity_type")
public class FinancialEntityType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_financial_entity_type", nullable = false)
    private int idFinancialEntityType;

    @NotNull(message = "financialEntityTypeName - no puede ser null")
    @NotEmpty(message = "financialEntityTypeName - no puede estar vacío")
    @Size(min = 1, max = 60, message = "financialEntityTypeName - máximo 60 caracteres")
    @Column(name = "financial_entity_type_name")
    private String financialEntityTypeName;

    @Valid
    @OneToMany(mappedBy = "financialEntityType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<FinancialEntity> financialEntities;

    public void addFinancialEntity(FinancialEntity financialEntity){
        if (financialEntity == null){
            return;
        }
        financialEntity.setFinancialEntityType(this);
        if (financialEntities == null){
            financialEntities = new ArrayList<>();
            financialEntities.add(financialEntity);
        }else if (!financialEntities.contains(financialEntity)){
            financialEntities.add(financialEntity);
        }
    }

    public FinancialEntityType(String financialEntityTypeName) {
        this.financialEntityTypeName = financialEntityTypeName;
    }
}
