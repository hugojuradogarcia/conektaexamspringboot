package com.conekta.antifraud.api.model.entity.exception.customvalidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NumberCardValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NumberCardConstraint {
    String message() default "cardNumber - Numero de tarjeta invalido (Luhn)";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
