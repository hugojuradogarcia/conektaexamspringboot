package com.conekta.antifraud.api.model.entity.exception.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NumberCardValidator implements ConstraintValidator<NumberCardConstraint, String> {
    @Override
    public boolean isValid(String ccNumber, ConstraintValidatorContext constraintValidatorContext) {
        int sum = 0;
        boolean alternate = false;
        for (int i = ccNumber.length() - 1; i >= 0; i--)
        {
            int n = Integer.parseInt(ccNumber.substring(i, i + 1));
            if (alternate)
            {
                n *= 2;
                if (n > 9)
                {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }

    @Override
    public void initialize(NumberCardConstraint constraintAnnotation) {

    }
}
