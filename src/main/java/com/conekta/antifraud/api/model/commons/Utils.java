package com.conekta.antifraud.api.model.commons;

import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.model.out.ResponseReport;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class Utils {

    public static String statusTextOk;
    public static String statusTextError;
    public static String messageOk;
    public static String messageOkSave;
    public static String messageNotFound;
    public static String messageExists;
    public static String messageDelete;
    public static String messageUpdate;
    public static String messageAssociation;

    @Value("${httpStatus.ok.statusText}")
    public void setStatusTextOk(String statusText) {
        Utils.statusTextOk = statusText;
    }
    @Value("${httpStatus.ok.messages}")
    public void setMessageOk(String messages) {
        Utils.messageOk = messages;
    }

    @Value("${httpStatus.ok.messagesSave}")
    public void setMessageOkSave(String messagesSave) {
        Utils.messageOkSave = messagesSave;
    }

    @Value("${httpStatus.ok.messagesNotFound}")
    public void setMessageNotFound(String messagesNotFound) {
        Utils.messageNotFound = messagesNotFound;
    }

    @Value("${httpStatus.ok.messagesExists}")
    public void setMessageExists(String messagesExists) {
        Utils.messageExists = messagesExists;
    }

    @Value("${httpStatus.badRequest.statusText}")
    public void setStatusTextError(String statusText) {
        Utils.statusTextError = statusText;
    }

    @Value("${httpStatus.ok.messagesDelete}")
    public void setMessagesDelete(String messagesDelete) {
        Utils.messageDelete = messagesDelete;
    }

    @Value("${httpStatus.ok.messagesUpdate}")
    public void setMessageUpdate(String messagesUpdate) {
        Utils.messageUpdate = messagesUpdate;
    }

    @Value("${httpStatus.ok.messagesAssociation}")
    public void setMessageAssociation(String messagesAssociation) {
        Utils.messageAssociation = messagesAssociation;
    }

    public final Response RESPONSE_OK_SAVE(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextOk)
                .message(messageOkSave)
                .build();
    }

    public final Response RESPONSE_NOT_FOUND(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextError)
                .message(messageNotFound)
                .build();
    }

    public final Response RESPONSE_EXIST(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextError)
                .message(messageExists)
                .build();
    }

    public final Response RESPONSE_OK_DELETE(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextError)
                .message(messageDelete)
                .build();
    }

    public final Response RESPONSE_OK_UPDATE(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextOk)
                .message(messageUpdate)
                .build();
    }

    public final Response RESPONSE_ERROR_ASSOCIATION(String status) {
        return Response.builder()
                .status(status)
                .statusText(statusTextOk)
                .message(messageAssociation)
                .build();
    }

    public final Response RESPONSE_FIND_REPORT(String status, ResponseReport responseReport) {
        return Response.builder()
                .status(status)
                .statusText(statusTextOk)
                .message(messageOk)
                .responseReport(responseReport)
                .build();
    }
}

