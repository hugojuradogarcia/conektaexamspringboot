package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.model.entity.Card;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.http.ResponseEntity;

public interface CardService {
    ResponseEntity<Response> updateCard(int id, Card card);
    ResponseEntity<Response> deleteCard(int id);
}
