package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Card;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.service.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

@RestController
@Api(description = "USER ONLY ACCESS - Update & Delete Card")
public class CardController {

    @Autowired
    private CardService cardService;

    @PutMapping("/user/updateCard/{id}")
    @ApiOperation(value = "Update Card")
    public @ResponseBody
    ResponseEntity<Response> updateCard(@PathVariable @Validated @Size(min = 1, message = "idCard - únicamente números, mínimo 1 número") int id,
                                        @RequestBody @Validated Card request){
        return cardService.updateCard(id, request);
    }

    @DeleteMapping("/user/deleteCard/{id}")
    @ApiOperation(value = "Delete Card")
    public @ResponseBody
    ResponseEntity<Response> deleteCard(@PathVariable @Validated @Size(min = 1, message = "idCard - únicamente números, mínimo 1 número") int id){
        return cardService.deleteCard(id);
    }
}
