package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.Report;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;

@Validated
@RestController
@Api(description = "USER ONLY ACCESS - Find Report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @PostMapping("/user/report/saveReport")
    @ApiOperation(value = "Save Report")
    public @ResponseBody
    ResponseEntity<Response> saveReport(@RequestBody @Validated Report request){
        return reportService.saveReport(request);
    }

    @GetMapping("/user/report/findByRfcAndCardNumberFinal")
    @ApiOperation(value = "Find Report By RFC & 4 last digits card number")
    public @ResponseBody
    ResponseEntity<Response> findByRfcAndCardNumberFinal(@Pattern(regexp = "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$", message = "rfc - debe de ser un rfc valido")
                                                         @RequestParam("rfc") String rfc,
                                                         @Pattern(regexp="(^$|[0-9]{4})", message = "cardNumber - únicamente los último 4 números")
                                                         @RequestParam("cardNumber") String cardNumber){
        return reportService.findByRfcAndCardNumberFinal(rfc, cardNumber);
    }

    @GetMapping("/user/report/findByRfc")
    @ApiOperation(value = "Find Report By RFC")
    public @ResponseBody
    ResponseEntity<Response> findByRfc(@Pattern(regexp = "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$", message = "rfc - debe de ser un rfc valido")
                                       @RequestParam("rfc") String rfc){
        return reportService.findByRfc(rfc);
    }

    @GetMapping("/user/report/findByCardNumberFinal")
    @ApiOperation(value = "Find Report 4 last digits card number")
    public @ResponseBody
    ResponseEntity<Response> findByCardNumberFinal(@Pattern(regexp="(^$|[0-9]{4})", message = "cardNumber - únicamente los último 4 números")
                                                   @RequestParam("cardNumber") String cardNumber){
        return reportService.findByCardNumberFinal(cardNumber);
    }
}
