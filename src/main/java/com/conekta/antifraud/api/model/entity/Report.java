package com.conekta.antifraud.api.model.entity;

import com.conekta.antifraud.api.model.entity.exception.customvalidator.StatusReportConstraint;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "report")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_report", nullable = false)
    @JsonIgnore
    private int idReport;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createdAt = new Date();

    @Column(name = "updated_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date updatedAt = new Date();

    @NotNull(message = "status - no puede ser null")
    @NotEmpty(message = "status - no puede estar vacío")
    /*@Size(min = 1, max = 50, message = "status - máximo 60 caracteres")*/
    @StatusReportConstraint
    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "card_holder_id",  insertable = true)
    private int cardHolderId;

    public Report(@NotNull(message = "status - no puede ser null") @NotEmpty(message = "status - no puede estar vacío") @Size(min = 1, max = 50, message = "status - máximo 60 caracteres") String status, int cardHolderId) {
        this.status = status;
        this.cardHolderId = cardHolderId;
    }
}
