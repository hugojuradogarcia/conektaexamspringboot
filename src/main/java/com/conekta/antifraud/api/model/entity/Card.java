package com.conekta.antifraud.api.model.entity;

import com.conekta.antifraud.api.model.entity.exception.customvalidator.NumberCardConstraint;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "card")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_card", nullable = false)
    private int idCard;

    @NotNull(message = "amountCredit - no puede ser null")
    @NotEmpty(message = "amountCredit - no puede estar vacío")
    @Pattern(regexp="(^$|[0-9]{1,50}+([.][0-9]{1,2}))", message = "amountCredit - únicamente números con mínimo 1 decimal máximo dos decimales")
    @Column(name = "amount_credit", nullable = false)
    private String amountCredit;

    @NotNull(message = "amountCreditUse - no puede ser null")
    @NotEmpty(message = "amountCreditUse - no puede estar vacío")
    @Pattern(regexp = "(^$|[0-9]{1,50}+([.][0-9]{1,2}))", message = "amountCreditUse - únicamente números con mínimo 1 decimal máximo dos decimales")
    @Column(name = "amount_credit_use", nullable = false)
    private String amountCreditUse;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @NotNull(message = "cardNumber - no puede ser null")
    @NumberCardConstraint
    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_holder_id",  insertable = true)
    @JsonBackReference
    private Cardholder cardholder;

    public Card(@NotNull(message = "amountCredit - no puede ser null") @NotEmpty(message = "amountCredit - no puede estar vacío") @Pattern(regexp = "(^$|[0-9]{1,50}+([.][0-9]{1,2}))", message = "amountCredit - únicamente números con mínimo 1 decimal máximo dos decimales") String amountCredit, @NotNull(message = "amountCreditUse - no puede ser null") @NotEmpty(message = "amountCreditUse - no puede estar vacío") @Pattern(regexp = "(^$|[0-9]{1,50}+([.][0-9]{1,2}))", message = "amountCreditUse - únicamente números con mínimo 1 decimal máximo dos decimales") String amountCreditUse, @NotNull(message = "cardNumber - no puede ser null") String cardNumber) {
        this.amountCredit = amountCredit;
        this.amountCreditUse = amountCreditUse;
        this.cardNumber = cardNumber;
    }
}
