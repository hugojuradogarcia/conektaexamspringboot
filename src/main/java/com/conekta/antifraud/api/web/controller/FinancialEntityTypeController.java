package com.conekta.antifraud.api.web.controller;

import com.conekta.antifraud.api.model.entity.FinancialEntityType;
import com.conekta.antifraud.api.model.out.Response;
import com.conekta.antifraud.api.service.FinancialEntityTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

@RestController
@Api(description = "ADMIN ONLY ACCESS - Save & Update Financial Entity Type")
public class FinancialEntityTypeController {

    @Autowired
    private FinancialEntityTypeService financialEntityTypeService;

    @PostMapping("/admin/saveFinancialEntityTypeAll")
    @ApiOperation(value = "Save Financial Entity Type All")
    public @ResponseBody
    ResponseEntity<Response> saveFinancialEntityTypeAll(@RequestBody @Validated FinancialEntityType request
    ){
        return financialEntityTypeService.saveFinancialEntityTypeAll(request);
    }

    @PostMapping("/admin/saveFinancialEntityType")
    @ApiOperation(value = "Save Financial Entity Type")
    public @ResponseBody
    ResponseEntity<Response> saveFinancialEntityType(@RequestBody @Validated FinancialEntityType request){
        return financialEntityTypeService.saveFinancialEntityType(request);
    }

    @PutMapping("/admin/updateFinancialEntityType/{id}")
    @ApiOperation(value = "Update Financial Entity Type All")
    public @ResponseBody
    ResponseEntity<Response> updateFinancialEntityType(@PathVariable @Validated @Size(min = 1, message = "idFinancialEntityType - únicamente números, mínimo 1 número") int id,
                                                       @RequestBody @Validated FinancialEntityType request){
        return financialEntityTypeService.updateFinancialEntityType(id, request);
    }
}
