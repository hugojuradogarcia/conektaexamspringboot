package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.FinancialEntityRepository;
import com.conekta.antifraud.api.model.commons.Utils;
import com.conekta.antifraud.api.model.entity.FinancialEntity;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FinancialEntityServiceImpl implements FinancialEntityService {

    @Autowired
    private Utils utils;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public ResponseEntity<Response> updateFinancialEntity(int id, FinancialEntity financialEntity) {
        Optional<FinancialEntity> financialEntityExists = financialEntityRepository.findById(id);

        if (financialEntityExists.isPresent()){
            financialEntity.setIdFinancialEntity(id);
            financialEntityRepository.save(financialEntity);
            return new ResponseEntity<>(utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> deleteFinancialEntity(int id) {
        Optional<FinancialEntity> financialEntityExists = financialEntityRepository.findById(id);

        if (financialEntityExists.isPresent()){
            financialEntityRepository.deleteById(id);
            return new ResponseEntity<>(utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
}
