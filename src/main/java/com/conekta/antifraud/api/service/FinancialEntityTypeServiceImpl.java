package com.conekta.antifraud.api.service;

import com.conekta.antifraud.api.dao.FinancialEntityRepository;
import com.conekta.antifraud.api.dao.FinancialEntityTypeRepository;
import com.conekta.antifraud.api.model.commons.Utils;
import com.conekta.antifraud.api.model.entity.FinancialEntity;
import com.conekta.antifraud.api.model.entity.FinancialEntityType;
import com.conekta.antifraud.api.model.out.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FinancialEntityTypeServiceImpl implements FinancialEntityTypeService {

    @Autowired
    private Utils utils;

    @Autowired
    private FinancialEntityTypeRepository financialEntityTypeRepository;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public ResponseEntity<Response> saveFinancialEntityTypeAll(FinancialEntityType financialEntityType) {

        // Validando que el tipo de la entidad financiera exista
        FinancialEntityType financialEntityTypeExist = financialEntityTypeRepository.findByFinancialEntityTypeName(financialEntityType.getFinancialEntityTypeName());
        List<FinancialEntity> financialEntityList = financialEntityType.getFinancialEntities();
        List<FinancialEntity> financialEntitieListSave = new ArrayList<>();
        List<FinancialEntity> financialEntitiesExist = financialEntityRepository.findAll();
        FinancialEntityType financialEntitiesTypePersist = new FinancialEntityType();
        boolean existItem;

        // Si no hay valores que insertar  en la lista de entidades
        if (financialEntityList != null) {

            for (FinancialEntity financialEntity:
                    financialEntityList) {
                existItem = false;
                for (FinancialEntity financialEntityExist:
                        financialEntitiesExist) {

                    if (financialEntityTypeExist != null) {
                        if (financialEntity.getFinancialName().equals(financialEntityExist.getFinancialName()) &&
                                financialEntityTypeExist.getIdFinancialEntityType() == financialEntityExist.getFinancialEntityType().getIdFinancialEntityType()) {
                            existItem = true;
                            break;
                        }
                    }else{
                        if (financialEntity.getFinancialName().equals(financialEntityExist.getFinancialName()) &&
                                financialEntityType.getIdFinancialEntityType() == financialEntityExist.getFinancialEntityType().getIdFinancialEntityType()) {
                            existItem = true;
                            break;
                        }
                    }
                }

                if (!existItem){
                    financialEntitieListSave.add(financialEntity);
                }
            }

            // Si no existe
            if (financialEntitieListSave.size()>0) {
                financialEntitiesTypePersist.setFinancialEntityTypeName(financialEntityType.getFinancialEntityTypeName());
                for (FinancialEntity financialEntity : financialEntitieListSave
                ) {
                    financialEntitiesTypePersist.addFinancialEntity(financialEntity);
                }

                financialEntityTypeRepository.save(financialEntitiesTypePersist);
                return new ResponseEntity<>(utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString()), HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(utils.RESPONSE_EXIST(HttpStatus.OK.toString()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> updateFinancialEntityType(int id, FinancialEntityType financialEntityType) {

        Optional<FinancialEntityType> financialEntityTypeExists = financialEntityTypeRepository.findById(id);

        if (financialEntityTypeExists.isPresent()){
            financialEntityType.setIdFinancialEntityType(id);
            financialEntityTypeRepository.save(financialEntityType);
            return new ResponseEntity<>(utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_NOT_FOUND(HttpStatus.OK.toString()), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Response> saveFinancialEntityType(FinancialEntityType financialEntityType) {

        FinancialEntityType financialEntityTypeExists = financialEntityTypeRepository.findByFinancialEntityTypeName(financialEntityType.getFinancialEntityTypeName());

        if (financialEntityTypeExists == null){
            financialEntityTypeRepository.save(financialEntityType);
            return new ResponseEntity<>(utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString()), HttpStatus.OK);
        }

        return new ResponseEntity<>(utils.RESPONSE_EXIST(HttpStatus.OK.toString()), HttpStatus.OK);
    }

    @Override
    public boolean findByName(String name) {
        FinancialEntityType financialEntityTypeExists = financialEntityTypeRepository.findByFinancialEntityTypeName(name);
        if (financialEntityTypeExists != null){
            return true;
        }
        return false;
    }

}
